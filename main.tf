provider "azurerm" {
  features {}
  subscription_id = "b473335b-e612-444d-a496-6ce2680a7f69"
  tenant_id       = "19d1096c-25fd-41f8-bd2a-8f37e47d0410"
  client_id       = "804fcdeb-3d1a-4c48-9fa7-abe23c13416d"
  client_secret   = "POn8Q~OGd3MXeQ2JEnWzQD2S6IxY2YmFO6Rq5c6a"
}


resource "azurerm_storage_account" "sa1" {
  name                     = "jayastoragesme"
  resource_group_name      = "TerraformRG"
  location                 = "East US"
  account_tier             = "Standard"
  account_replication_type = "LRS"
}


resource "azurerm_storage_container" "cont1" {
  name                  = "jayaconatinersme"
  storage_account_name  = azurerm_storage_account.sa1.name
  container_access_type = "private"
}

resource "azurerm_storage_blob" "blob1" {
  name                   = "jayastorageblobsme"
  storage_account_name   = azurerm_storage_account.sa1.name
  storage_container_name = azurerm_storage_container.cont1.name
  type                   = "Block"
  source                 = "C:/TerraformAzureWS/Demo1StorageAccount/Demo.zip"
}
